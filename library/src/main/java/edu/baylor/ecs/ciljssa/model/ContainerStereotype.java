package edu.baylor.ecs.ciljssa.model;

public enum ContainerStereotype {
    FABRICATED, CONTROLLER, SERVICE, RESPONSE, ENTITY, REPOSITORY, BEAN, MODULE;
}
