package edu.baylor.ecs.ciljssa.model;

public enum ClassOrInterface {
    CLASS,
    MODULE,
    INTERFACE;
}
