package edu.baylor.ecs.ciljssa.app.response;

public enum ResponseType {
    SUCCESS,
    FAIL;
}
